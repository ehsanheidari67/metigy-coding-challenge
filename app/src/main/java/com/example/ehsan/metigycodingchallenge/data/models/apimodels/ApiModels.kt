package com.example.ehsan.metigycodingchallenge.data.models.apimodels

import com.example.ehsan.metigycodingchallenge.core.extension.ofApiDateTime
import com.example.ehsan.metigycodingchallenge.data.models.appmodels.NewsItemModel
import com.example.ehsan.metigycodingchallenge.data.models.appmodels.WeatherUiModel
import java.time.LocalDateTime

data class NewsListModel(
    val articles: List<Article>? = null,
    val status: String? = null,
    val totalResults: Int? = null
)

data class Article(
    val author: String? = null,
    val content: String? = null,
    val description: String? = null,
    val publishedAt: String? = null,
    val source: Source? = null,
    val title: String? = null,
    val url: String? = null,
    val urlToImage: String? = null
) {
    fun toNewsItem(): NewsItemModel =
        NewsItemModel(
            title = title ?: "",
            description = description ?: "",
            date = publishedAt?.ofApiDateTime(),
            imageUrl = urlToImage ?: ""
        )
}

data class Source(
    val id: Any? = null,
    val name: String? = null
)

data class WeatherModel(
    val base: String? = null,
    val clouds: Clouds? = null,
    val cod: Int? = null,
    val coord: Coord? = null,
    val dt: Int? = null,
    val id: Int? = null,
    val main: Main? = null,
    val name: String? = null,
    val sys: Sys? = null,
    val timezone: Int? = null,
    val visibility: Int? = null,
    val weather: List<Weather>? = null,
    val wind: Wind? = null
) {
    fun toWeatherUiModel(): WeatherUiModel = WeatherUiModel(
        temp = main?.temp?.toString() ?: "",
        minTemp = main?.temp_min?.toString() ?: "",
        maxTemp = main?.temp_max?.toString() ?: ""
    )
}

data class Clouds(
    val all: Int? = null
)

data class Coord(
    val lat: Double? = null,
    val lon: Double? = null
)

data class Main(
    val feels_like: Double? = null,
    val humidity: Int? = null,
    val pressure: Int? = null,
    val temp: Double? = null,
    val temp_max: Double? = null,
    val temp_min: Double? = null
)

data class Sys(
    val country: String? = null,
    val id: Int? = null,
    val sunrise: Int? = null,
    val sunset: Int? = null,
    val type: Int? = null
)

data class Weather(
    val description: String? = null,
    val icon: String? = null,
    val id: Int? = null,
    val main: String? = null
)

data class Wind(
    val deg: Int? = null,
    val speed: Double? = null
)