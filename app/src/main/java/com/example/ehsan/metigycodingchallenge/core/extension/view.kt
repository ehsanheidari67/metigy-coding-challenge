package com.example.ehsan.metigycodingchallenge.core.extension

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

private fun ViewGroup?.inflateLayout(resource: Int, attachToRoot: Boolean): View =
    LayoutInflater.from(this?.context).inflate(resource, this, attachToRoot)

fun ViewGroup?.inflateRecyclerAdapterLayout(resource: Int): View = inflateLayout(resource, false)
