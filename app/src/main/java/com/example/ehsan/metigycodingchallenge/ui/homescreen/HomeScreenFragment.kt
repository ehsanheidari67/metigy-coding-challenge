package com.example.ehsan.metigycodingchallenge.ui.homescreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ehsan.metigycodingchallenge.R
import com.example.ehsan.metigycodingchallenge.databinding.FragmentHomeScreenBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeScreenFragment : Fragment() {

    private val viewModel: HomeScreenViewModel by viewModels()

    private lateinit var binding :FragmentHomeScreenBinding

    private val adapter : NewsAdapter by lazy {
        NewsAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUi()
        startObserving()
    }

    private fun startObserving(){
        viewModel.newsList.observe(viewLifecycleOwner){newsListModel->
            adapter.data = newsListModel.map {
                it.toNewsItem()
            }
        }

        viewModel.weather.observe(viewLifecycleOwner){weatherModel ->
            weatherModel.toWeatherUiModel().let {
                binding.minTemp.text = getString(R.string.min_temp) + it.minTemp
                binding.temp.text = getString(R.string.temp) + it.temp
                binding.maxTemp.text = getString(R.string.max_temp) + it.maxTemp
            }
        }

        viewModel.loading.observe(viewLifecycleOwner){loading->
            if (loading){
                binding.loading.visibility = View.VISIBLE
            }else{
                binding.loading.visibility = View.GONE
            }
        }
    }

    private fun setupUi(){
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
    }
}