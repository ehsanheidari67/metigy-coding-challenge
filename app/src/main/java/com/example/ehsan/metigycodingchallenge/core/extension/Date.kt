package com.example.ehsan.metigycodingchallenge.core.extension

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

fun String.toDateTime(format: String): LocalDateTime {
    val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern(format)
    return LocalDateTime.parse(this, formatter)
}

fun String.ofApiDateTime(): LocalDateTime = toDateTime("yyyy-MM-dd'T'HH:mm:ss'Z'")

fun LocalDateTime.newsUiFormat(): String {
    val formatter = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm:ss")
    return format(formatter)
}

fun LocalDate.newsApiQueryParamFormat(): String{
    val formatter = DateTimeFormatter.ofPattern("yy-MM-dd")
    return format(formatter)
}