package com.example.ehsan.metigycodingchallenge.data.models.appmodels

import java.time.LocalDateTime

data class NewsItemModel(
    val title: String,
    val description: String,
    val date: LocalDateTime?,
    val imageUrl: String
)

data class WeatherUiModel(val temp: String, val minTemp: String, val maxTemp: String)