package com.example.ehsan.metigycodingchallenge.data.weather

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.ehsan.metigycodingchallenge.data.api.WeatherService
import com.example.ehsan.metigycodingchallenge.data.models.apimodels.WeatherModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import com.example.ehsan.metigycodingchallenge.core.result.Result

interface WeatherRepository {
    fun weather():LiveData<Result<WeatherModel>>
}

class WeatherRepositoryDefault @Inject constructor(private val service:WeatherService) : WeatherRepository{
    override fun weather(): LiveData<Result<WeatherModel>> {
        val result = MutableLiveData<Result<WeatherModel>>()
        result.value = Result.Loading
        service.weatherInfo().enqueue(object : Callback<WeatherModel>{

            override fun onResponse(call: Call<WeatherModel>, response: Response<WeatherModel>) {
                if (response.isSuccessful){
                    response.body()?.let {
                        result.value = Result.Success(it)
                    }?:run {
                        result.value = Result.Error(Exception("Network Error"))
                    }
                }else{
                    result.value = Result.Error(Exception("Network Error"))
                }
            }

            override fun onFailure(call: Call<WeatherModel>, t: Throwable) {
                result.value = Result.Error(Exception("Network Error"))
            }

        })
        return result
    }
}