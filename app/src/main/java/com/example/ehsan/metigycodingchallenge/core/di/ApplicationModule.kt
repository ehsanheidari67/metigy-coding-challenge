package com.example.ehsan.metigycodingchallenge.core.di

import android.app.Application
import android.content.Context
import com.example.ehsan.metigycodingchallenge.data.api.NewsService
import com.example.ehsan.metigycodingchallenge.data.api.WeatherService
import com.example.ehsan.metigycodingchallenge.data.news.NewsRepository
import com.example.ehsan.metigycodingchallenge.data.news.NewsRepositoryDefault
import com.example.ehsan.metigycodingchallenge.data.weather.WeatherRepository
import com.example.ehsan.metigycodingchallenge.data.weather.WeatherRepositoryDefault
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl("http://newsapi.org")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideNewsService(): NewsService = Retrofit.Builder()
        .baseUrl("http://newsapi.org")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(NewsService::class.java)

    @Provides
    @Singleton
    fun provideWeatherService():WeatherService = Retrofit.Builder()
        .baseUrl("http://api.openweathermap.org")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(WeatherService::class.java)

    @Provides
    @Singleton
    fun provideNewsRepository(newsRepository: NewsRepositoryDefault): NewsRepository =
        newsRepository

    @Provides
    @Singleton
    fun provideWeatherRepository(weatherRepository: WeatherRepositoryDefault): WeatherRepository =
        weatherRepository
}