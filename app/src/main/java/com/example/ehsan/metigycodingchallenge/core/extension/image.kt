package com.example.ehsan.metigycodingchallenge.core.extension

import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.setImage(url: String? = null) {
    Glide.with(context).load(url).into(this)
}
