package com.example.ehsan.metigycodingchallenge.ui.homescreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.example.ehsan.metigycodingchallenge.core.result.Result
import com.example.ehsan.metigycodingchallenge.data.models.apimodels.Article
import com.example.ehsan.metigycodingchallenge.data.models.apimodels.NewsListModel
import com.example.ehsan.metigycodingchallenge.data.models.apimodels.WeatherModel
import com.example.ehsan.metigycodingchallenge.data.models.appmodels.WeatherUiModel
import com.example.ehsan.metigycodingchallenge.data.news.NewsRepository
import com.example.ehsan.metigycodingchallenge.data.weather.WeatherRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class HomeScreenViewModel @Inject constructor(
    private val newsRepository: NewsRepository,
    private val weatherRepository: WeatherRepository
) : ViewModel() {
    private val _newsList = MediatorLiveData<List<Article>>()
    val newsList: LiveData<List<Article>>
        get() = _newsList

    private val _weather = MediatorLiveData<WeatherModel>()
    val weather: LiveData<WeatherModel>
        get() = _weather

    private val _loading = MediatorLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading


    init {
        getNewsList()
        getWeather()
    }

    private fun getNewsList() {
        _loading.value = true
        _newsList.addSource(newsRepository.news()) { result ->
            if (result is Result.Success) {
                _loading.value = false
                _newsList.value = result.data
            } else if (result is Result.Error) {
                Timber.e(result.error)
            }
        }
    }

    private fun getWeather() {
        _weather.addSource(weatherRepository.weather()) { result ->
            if (result is Result.Success){
                _weather.value = result.data
            }else if (result is Result.Error){
                Timber.e(result.error)
            }
        }
    }
}