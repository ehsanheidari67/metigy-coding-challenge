package com.example.ehsan.metigycodingchallenge.data.news

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.ehsan.metigycodingchallenge.core.extension.newsApiQueryParamFormat
import com.example.ehsan.metigycodingchallenge.core.result.Result
import com.example.ehsan.metigycodingchallenge.data.api.NewsService
import com.example.ehsan.metigycodingchallenge.data.models.apimodels.Article
import com.example.ehsan.metigycodingchallenge.data.models.apimodels.NewsListModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDate
import javax.inject.Inject

const val NETWORK_ERROR = "Network Error"

interface NewsRepository {
    fun news(): LiveData<Result<List<Article>>>
}

class NewsRepositoryDefault @Inject constructor(private val newsService: NewsService) :
    NewsRepository {
    override fun news(): LiveData<Result<List<Article>>> {
        val result = MutableLiveData<Result<List<Article>>>()
        result.value = Result.Loading

        newsService.newsList(LocalDate.now().newsApiQueryParamFormat()).enqueue(object : Callback<NewsListModel> {
            override fun onResponse(call: Call<NewsListModel>, response: Response<NewsListModel>) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        result.value = Result.Success(it.articles?: listOf())
                    } ?: run {
                        result.value = Result.Error(Exception(NETWORK_ERROR))
                    }
                } else {
                    result.value = Result.Error(Exception(NETWORK_ERROR))
                }
            }

            override fun onFailure(call: Call<NewsListModel>, t: Throwable) {
                result.value = Result.Error(Exception(NETWORK_ERROR))
            }

        })

        return result
    }
}