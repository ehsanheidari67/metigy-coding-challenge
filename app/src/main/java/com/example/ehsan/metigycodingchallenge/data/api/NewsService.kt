package com.example.ehsan.metigycodingchallenge.data.api

import com.example.ehsan.metigycodingchallenge.data.models.apimodels.NewsListModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsService {

    @GET("/v2/everything?q=tesla&sortBy=publishedAt&apiKey=a0c4727eb28e4afea59164ccb14f391f")
    fun newsList(@Query("from") from:String) : Call<NewsListModel>
}