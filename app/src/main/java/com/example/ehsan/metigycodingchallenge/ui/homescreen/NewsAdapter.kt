package com.example.ehsan.metigycodingchallenge.ui.homescreen

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ehsan.metigycodingchallenge.R
import com.example.ehsan.metigycodingchallenge.core.extension.inflateRecyclerAdapterLayout
import com.example.ehsan.metigycodingchallenge.core.extension.newsUiFormat
import com.example.ehsan.metigycodingchallenge.core.extension.setImage
import com.example.ehsan.metigycodingchallenge.data.models.appmodels.NewsItemModel
import com.example.ehsan.metigycodingchallenge.databinding.ItemNewsBinding

class NewsAdapter : RecyclerView.Adapter<NewsAdapter.NewsItemViewHolder>() {

    var data: List<NewsItemModel> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsItemViewHolder =
        NewsItemViewHolder(ItemNewsBinding.bind(parent.inflateRecyclerAdapterLayout(R.layout.item_news)))

    override fun onBindViewHolder(holder: NewsItemViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    inner class NewsItemViewHolder(private val binding: ItemNewsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: NewsItemModel) {
            binding.newsTitle.text = item.title
            binding.newsDescription.text = item.description
            binding.newsImageView.setImage(item.imageUrl)
            binding.date.text = item.date?.newsUiFormat()
        }
    }
}