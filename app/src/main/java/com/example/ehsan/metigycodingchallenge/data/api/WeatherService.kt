package com.example.ehsan.metigycodingchallenge.data.api

import com.example.ehsan.metigycodingchallenge.data.models.apimodels.WeatherModel
import retrofit2.Call
import retrofit2.http.GET

interface WeatherService {
    @GET("/data/2.5/weather?q=Sydney,au&APPID=e09fedce0b7671e2871c01d9b0fdfde2")
    fun weatherInfo(): Call<WeatherModel>
}