package com.example.ehsan.metigycodingchallenge.core.result

sealed class Result<out R> {
    data class  Success<T>(val data:T) : Result<T>()
    data class Error(val error:Exception) : Result<Nothing>()
    object Loading : Result<Nothing>()
}